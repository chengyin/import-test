# Linking Tests

## Good links

- [Link externally](https://slab.com)
- [Link internally to another doc](sub sub.md)
- [Link internally to another doc's anchor](sub sub.md#anchor-2)
- [Link internally to another doc's anchor](../../LINK2.md#anchor-2)
- [Link internally to another doc's anchor](../../LINK2.md#anchor-2)
- [Link in self to an anchor](#this-is-an-anchor)
- [Link to a file](../stark.png)
- [Link to a file](./image.png)
- [Link to email](mailto:chengyin@slab.com)
- Inline formatting (formatting won't work): [**Not bold link **](../../LINKS2.md)

## Bad links

- [Link going nowhere](nowhere)
- [Link going nowhere.md](nowhere.md)
- [Link going to anchor not exist](#n0te3xist)

## This is an Anchor
